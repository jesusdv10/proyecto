<?php 
/*
*
*	***** Pasarela Woocommerce *****
*
*	This file initializes all VEXSOLUCIONES_ Core components
*	
*/
// If this file is called directly, abort. //
if ( ! defined( 'WPINC' ) ) {die;} // end if
// Define Our Constants
define('VEXSOLUCIONES__CORE_INC',dirname( __FILE__ ).'/assets/inc/');
define('VEXSOLUCIONES__CORE_IMG',plugins_url( 'assets/img/', __FILE__ ));
define('VEXSOLUCIONES__CORE_CSS',plugins_url( 'assets/css/', __FILE__ ));
define('VEXSOLUCIONES__CORE_JS',plugins_url( 'assets/js/', __FILE__ ));
/*
*
*  Register CSS
*
*/
function vexsoluciones__register_core_css(){
wp_enqueue_style('vexsoluciones_-core', VEXSOLUCIONES__CORE_CSS . 'vexsoluciones_-core.css',null,time(),'all');
};
add_action( 'wp_enqueue_scripts', 'vexsoluciones__register_core_css' );    
/*
*
*  Register JS/Jquery Ready
*
*/
function vexsoluciones__register_core_js(){
// Register Core Plugin JS	
wp_enqueue_script('vexsoluciones_-core', VEXSOLUCIONES__CORE_JS . 'vexsoluciones_-core.js','jquery',time(),true);
};
add_action( 'wp_enqueue_scripts', 'vexsoluciones__register_core_js' );    
/*
*
*  Includes
*
*/ 
// Load the Functions
if ( file_exists( VEXSOLUCIONES__CORE_INC . 'vexsoluciones_-core-functions.php' ) ) {
	require_once VEXSOLUCIONES__CORE_INC . 'vexsoluciones_-core-functions.php';
}     
// Load the ajax Request
if ( file_exists( VEXSOLUCIONES__CORE_INC . 'vexsoluciones_-ajax-request.php' ) ) {
	require_once VEXSOLUCIONES__CORE_INC . 'vexsoluciones_-ajax-request.php';
} 
// Load the Shortcodes
if ( file_exists( VEXSOLUCIONES__CORE_INC . 'vexsoluciones_-shortcodes.php' ) ) {
	require_once VEXSOLUCIONES__CORE_INC . 'vexsoluciones_-shortcodes.php';
}