<?php 
/*
*
*	***** Pasarela Woocommerce *****
*
*	Shortcodes
*	
*/
// If this file is called directly, abort. //
if ( ! defined( 'WPINC' ) ) {die;} // end if
/*
*
*  Build The Custom Plugin Form
*
*  Display Anywhere Using Shortcode: [vexsoluciones__custom_plugin_form]
*
*/
function vexsoluciones__custom_plugin_form_display($atts, $content = NULL){
		extract(shortcode_atts(array(
      	'el_class' => '',
      	'el_id' => '',	
		),$atts));    
        
        $out ='';
        $out .= '<div id="vexsoluciones__custom_plugin_form_wrap" class="vexsoluciones_-form-wrap">';
        $out .= 'Hey! Im a cool new plugin named <strong>Pasarela Woocommerce!</strong>';
        $out .= '<form id="vexsoluciones__custom_plugin_form">';
        $out .= '<p><input type="text" name="myInputField" id="myInputField" placeholder="Test Field: Test Ajax Responses"></p>';
        
        // Final Submit Button
        $out .= '<p><input type="submit" id="submit_btn" value="Submit My Form"></p>';        
        $out .= '</form>';
         // Form Ends
        $out .='</div><!-- vexsoluciones__custom_plugin_form_wrap -->';       
        return $out;
}
/*
Register All Shorcodes At Once
*/
add_action( 'init', 'vexsoluciones__register_shortcodes');
function vexsoluciones__register_shortcodes(){
	// Registered Shortcodes
	add_shortcode ('vexsoluciones__custom_plugin_form', 'vexsoluciones__custom_plugin_form_display' );
};