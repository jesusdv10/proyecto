<?php



/**
 * Plugin Name: Pasarela de Pago, Woocommerce
 * Plugin URI: http://localhost
 * Description: Pagar con tarjeta de crédito mediante la pasarela de pago
 * Version: 1.0.0
 * Author: Jesús Villegas
 *
 */

add_action( 'init', 'init_redsys' );
add_action( 'plugins_loaded', 'load_redsys' );

function init_redsys() {
    load_plugin_textdomain( "redsys", false, dirname( plugin_basename( __FILE__ ) ));
}

function load_redsys() {
    if ( !class_exists( 'WC_Payment_Gateway' ) ) 
        exit;

    include_once ('sistema.php');
	
    add_filter( 'woocommerce_payment_gateways', 'anadir_pago_woocommerce_redsys' );
}

function anadir_pago_woocommerce_redsys($methods) {
    $methods[] = 'WC_Redsys';
    return $methods;
}