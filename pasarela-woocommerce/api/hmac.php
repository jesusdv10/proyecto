<?php

include 'hash.php';


function php_compat_hash_hmac($algo, $data, $key, $raw_output = false)
{
    // Block size (byte) for MD5, SHA-1 and SHA-256.
    $blocksize = 64;

    
    $opad = str_repeat("\x5c", $blocksize);
    $ipad = str_repeat("\x36", $blocksize);

    if (strlen($key) > $blocksize) {
        $key = hash($algo, $key, true);
    } else {
        $key = str_pad($key, $blocksize, "\x00");
    }

    
    $opad ^= $key;
    $ipad ^= $key;

    return hash($algo, $opad . hash($algo, $ipad . $data, true), $raw_output);
}


// Define
if (!function_exists('hash_hmac4')) {
    function hash_hmac4($algo, $data, $key, $raw_output = false)
    {
        return php_compat_hash_hmac($algo, $data, $key, $raw_output);
    }
}
?>