<?php


function php_compat_hash($algo, $data, $raw_output = false)
{
    $algo = strtolower($algo);
    switch ($algo) {
               

        case 'sha256':
            require_once dirname(__FILE__) . '/sha256.php';
            $hash = SHA256::hash($data);
            break;

        case 'sha1':
            if (!function_exists('sha1')) {
                require dirname(__FILE__) . '/sha1.php';
            }
            $hash = sha1($data);
            break;    

        case 'md5':
            $hash = md5($data);
            break;    

        default:
            user_error('hash(): Unknown hashing algorithm: ' . $algo, E_USER_WARNING);
            return false;
    }

    if ($raw_output) {
        return pack('H*', $hash);
    } else {
        return $hash;
    }
}


// Define
if (!function_exists('hash')) {
    function hash($algo, $data, $raw_output = false)
    {
        return php_compat_hash($algo, $data, $raw_output);
    }
}
?>